﻿mooflow.classes.Wellgroup
=========================

.. currentmodule:: mooflow.classes

.. autoclass:: Wellgroup

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Wellgroup.__init__
      ~Wellgroup.add_wells
      ~Wellgroup.all_groups
      ~Wellgroup.all_id_groups
      ~Wellgroup.get_settings
      ~Wellgroup.idx_of_well
      ~Wellgroup.is_in_group
      ~Wellgroup.set_group_multiplier
      ~Wellgroup.set_l_u_boundaries
      ~Wellgroup.set_pywr_node
      ~Wellgroup.set_sector
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Wellgroup.num_par
   
   