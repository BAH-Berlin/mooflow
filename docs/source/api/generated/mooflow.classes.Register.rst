﻿mooflow.classes.Register
========================

.. currentmodule:: mooflow.classes

.. autoclass:: Register

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Register.__init__
      ~Register.add
      ~Register.discard
      ~Register.entries_in_rectangle
      ~Register.find_well_loc
   
   

   
   
   