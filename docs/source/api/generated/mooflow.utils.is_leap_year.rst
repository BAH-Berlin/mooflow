﻿mooflow.utils.is\_leap\_year
============================

.. currentmodule:: mooflow.utils

.. autofunction:: is_leap_year