﻿mooflow.classes.GW\_level\_pattern
==================================

.. currentmodule:: mooflow.classes

.. autoclass:: GW_level_pattern

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~GW_level_pattern.__init__
      ~GW_level_pattern.get_level
      ~GW_level_pattern.get_levels
      ~GW_level_pattern.set_pattern
   
   

   
   
   