﻿mooflow.classes.Well
====================

.. currentmodule:: mooflow.classes

.. autoclass:: Well

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Well.__init__
      ~Well.get_pattern_dem
      ~Well.get_pattern_ext
      ~Well.get_rate_dem
      ~Well.get_rate_ext
      ~Well.get_sum_year
      ~Well.is_active_on_date
      ~Well.set_pattern
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Well.active_on
      ~Well.location
      ~Well.multiplier
      ~Well.pywr_node
      ~Well.sector
      ~Well.type
   
   