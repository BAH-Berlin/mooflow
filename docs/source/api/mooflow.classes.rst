Analyse
=======

.. currentmodule:: mooflow.classes


Classes

.. autosummary::
   :toctree: generated/

   Extr_pattern
   GW_level_pattern
   Well
   Wellgroup
   Omo_setup
   Area_gw
   Register
   Pywr_model
   Parameters 
