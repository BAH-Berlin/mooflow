Funcs
=====

.. currentmodule:: mooflow.funcs


Functions to comupte fitness functions or indicators

.. autosummary::
   :toctree: generated/

   linear_slope
   days_violated
   max_len_ones
   sum_heads_fg
   count_neg_slopes
   sum_neg_slopes

   pywr_total__no_recorder_noparameters
