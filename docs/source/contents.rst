Welcome to Mooflow's documentation!
###################################

A generalized framework for basin-scale multi-objective simulation-optimization with MODFLOW

Mooflow helps you to setup and perform multi-objective simulation-based optimization of ground water resources management. 
Mooflow couples the groundwater model `MODFLOW <https://www.usgs.gov/mission-areas/water-resources/science/modflow-and-related-programs>`__
with the water distribution network model `Pywr <https://github.com/pywr/pywr>`__ and 
optimization algorithms from the `DEAP <https://github.com/DEAP/deap>`__ package.

.. image:: _static/MKSBO_eng.png


Basin-wide optimization can comprise many wells and a substantial
number of decision variables. To reduce optimization complexity,
Mooflow features methods to minimize the number of decision variable by grouping wells and sharing
decision variables. A short technical paper about Mooflow can be found at the `Medwater homepage <http://grow-medwater.de/home/wp-content/uploads/2020/10/TN_Moo.pdf>`__.

.. image:: _static/patterns.png

Code 
****

Mooflow is available at `bitbucket.org <https://bitbucket.org/BAH_Berlin/mooflow/>`__.

Contents:
*********

.. toctree::
   :maxdepth: 2

   Python API reference <api/mooflow.rst>
   tutorial.rst
   miscellaneous.rst
   license.rst
