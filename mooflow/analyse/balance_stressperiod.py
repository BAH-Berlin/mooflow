#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Read the lst file and compute the balance.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import numpy as np
from mooflow.utils import dy_datetime
from mooflow.read import lstfile_iter_balance


def list_file(setup_mf, reg_well):
    '''Read the balance sections of the list file.

    Parameters
    ----------
    setup_mf : mooflow.classes.Omo_setup
        the modflow setup
    reg_well : mooflow.classes.Registry for wells
        the registry for wells

    Returns
    -------
    dict : details the balance components'''
    dict_lst = {}
    iter_bal = lstfile_iter_balance(setup_mf, 
        period_number=setup_mf.setting_time, style='dict')


    len_per = setup_mf.number_timesteps - setup_mf.setting_time


    str_bal_array = np.zeros((3, len_per))
    str_drain = np.zeros(len_per)
    str_storage = np.zeros(len_per)

    date = setup_mf.starting_date

    for i, ts in enumerate(setup_mf.timesteps_enddates): #in enumerate(setup_mf.get_cum_per()[stime:-2]):
        str_per_dem = 0

        try:
            str_per_b = next(iter_bal)
        except StopIteration:
            print('list file ended prematurely in {} / {}'.format(i, ts))

        if i < setup_mf.setting_time:
            continue

        str_per_balance_in = str_per_b['WELLS IN']
        str_per_balance_out = str_per_b['WELLS OUT']
        str_drain[i-setup_mf.setting_time] = str_per_b['DRAINS OUT']
        str_storage[i-setup_mf.setting_time] = str_per_b['STORAGE OUT']
        
        for well in reg_well:
            if well.is_active_on_date(*dy_datetime(date)):
                str_per_dem += well.get_rate_dem(date)
        ### row 0: total of all extraction demands from wells
        ### row 1: READ total of all extractions from wells
        ### row 2: READ total of all infiltrations from wells
        str_bal_array[:, i-setup_mf.setting_time] = str_per_dem, str_per_balance_out, str_per_balance_in

    diff = str_bal_array[0,:] - str_bal_array[1,:]

    if any(diff < 0):
        dict_lst.update({'shortage max': max(diff[diff < 0])})
        dict_lst.update({'shortage sum': sum(diff[diff < 0])})
        dict_lst.update({'oversupply max': 0})
        dict_lst.update({'oversupply sum': 0})
    else:
        dict_lst.update({'shortage max': 0})
        dict_lst.update({'shortage sum': 0})
        dict_lst.update({'oversupply max': sum(diff[diff > 0])})
        dict_lst.update({'oversupply sum': sum(diff[diff > 0])})

    dict_lst.update({'balance array': str_bal_array})
    dict_lst.update({'drains array': str_drain})
    dict_lst.update({'storage array': str_storage})
    return dict_lst


if __name__ == "__main__":
    pass