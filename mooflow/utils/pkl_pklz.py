# -*- coding: utf-8 -*-
"""
Functions to save and load gzip compressed pickle files.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""
import gzip
import pickle
import sys

def save_obj_compressed(name, obj, ending='pklz'):
    '''Saves a struct or list into a prickle binary file.

    Parameters
    ----------
    name : string
        path and filename for prickle file
        without file ending, functions add .pkl
    obj : struct, list,...
        object to save in prickle file'''
    len_end = len(ending)
    if len(name) > len_end+2:
        if name[-len_end:] == ending:
            name = name[:-len_end-1]
    try:
        with gzip.open(name + '.' + ending, 'wb') as f:
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
            #print('saved')
    except Exception as e:
        raise IOError('could not write prickle file {} / {}'.format(name, e))


def load_obj_compressed(name, ending='pklz'):
    '''Loads a prickle binary file.

    Parameters
    ----------
    name : string
        path and filename for prickle file'''
    len_end = len(ending)
    if len(name) > len_end+2:
        if name[-len_end:] == ending:
            name = name[:-len_end-1]
    try:
        with gzip.open(name + '.' + ending, 'rb') as f:
            if sys.version_info[0] < 3:
                temp = pickle.load(f)
            else:
                temp = pickle.load(f, encoding='bytes')
            return temp
    except FileNotFoundError as e:
        raise e


def save_obj(name, obj, ending='pkl'):
    '''Save a struct or list into a prickle binary file.

    Parameters
    ----------
    name : string
        path and filename for prickle file
        without file ending, functions add .pkl
    obj : struct, list,...
        object to save in prickle file'''
    len_end = len(ending)
    if len(name) > len_end+2:
        if name[-len_end:] == ending:
            name = name[:-len_end-1]
    try:
        with open(name + '.' + ending, 'wb') as f:
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL) #L
            #print('saved')
    except:
        raise IOError('could not write prickle file {}'.format(name))


def load_obj(name, ending='pkl'):
    '''Load a prickle binary file.

    Parameters
    ----------
    name : string
        path and filename for prickle file'''
    len_end = len(ending)
    if len(name) > len_end+2:
        if name[-len_end:] == ending:
            name = name[:-len_end-1]
    try:
        with open(name + '.' + ending, 'rb') as f:
            if sys.version_info[0] < 3:
                return pickle.load(f)
            else:
                return pickle.load(f, encoding='bytes')
    except FileExistsError:
        raise FileExistsError('could not read prickle file {}'.format(name))

###############################################################################
###################### SECURITY ###############################################

if __name__ == '__main__':
    pass
