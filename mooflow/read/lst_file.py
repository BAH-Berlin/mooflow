# -*- coding: utf-8 -*-
"""
Functions to read the list file.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import os
import numpy as np

from mooflow.utils import make_shape_two
from mooflow.classes import Omo_setup


def lstfile_nwt_shortage(lst_file, lines_after_m1=None):
    """Generator function that scans the output of readlines on the results file \*.LIST
    to return an numpy array dtype float with the results for the current time step

    Parameters
    ----------
    lst_file : str or list of str
        the results file as read with readlines or the path to the file
    lines_after_m1 : int
        lines offset "OUTPUT CONTROL FOR STRESS PERIOD"""
    def read_line(line):
        marker_array = np.zeros((4))
        tmp = [float(x) for x in line.split()]
        marker_array[:3] = tmp[:3]
        marker_array[3] = float(tmp[3]) + float(tmp[4]) #[float(x) for x in line.split()[:3]]
        return marker_array

    if isinstance(lst_file, str):
        with open(lst_file, 'r') as fid:
            list_file = fid.readlines()
    elif isinstance(lst_file, Omo_setup):
        tmp = os.path.join(lst_file.modelfolder, lst_file.modelname + '.list')
        with open(tmp, 'r') as fid:
            list_file = fid.readlines()
    else:
        list_file = lst_file

    marker = "OUTPUT CONTROL FOR STRESS PERIOD"#
    marker_2 = "WELLS WITH REDUCED PUMPING FOR STRESS PERIOD"

    num1, num2 = None, None
    if lines_after_m1 is None:
        for lnr, line in enumerate(list_file):
            if marker in line:
                num1 = lnr
            if marker_2 in line:
                num2 = lnr
                break
        if num1 is None or num2 is None:
            raise ValueError("lst file is odd. check it.")
        lines_after_m1 = num2 - num1

    #filename_well = marker + buk.format.make_shape_two(tstep, length=4) + 'lst'
    read_well_cont = False
    start_at_line = -1
    start_at_line_2 = -1

    list_stress_short = []
    ts_info = None
    
    cnt = 0
    for lnr, line in enumerate(list_file):
        ### start
        if read_well_cont:
            try:
                marker_array = read_line(line)
                list_stress_short.append(marker_array)
            except:
                read_well_cont = False
                yield list_stress_short, ts_info
        else:
            if marker in line:
                list_stress_short = []
                ts_info = [int(s) for s in line.split() if s.isdigit()]
                start_at_line = lnr + lines_after_m1
                ####
                #### read stress periods from dis --> know which time step
                ####
                #ts_pe = [int(x) for x in re.findall(r"\d+", line)]
                #print('startline NO OUTPUT CONTROL at {} next is {}'.format(lnr, start_at_line))
            if lnr == start_at_line:
                cnt += 1
                if marker_2 in line:
                    start_at_line_2 = lnr + 2
                    #print('marker read at {} next is {}'.format(lnr, start_at_line_2))
            if lnr == start_at_line_2:
                read_well_cont = True
                #print(lnr - start_at_line)
                #marker_array[cnt, :2] = ts_pe
                marker_array = read_line(line)
                list_stress_short.append(marker_array)
                start_at_line = -1



def lstfile_iter_well_drn(lst_file, marker='WEL_', nrows=477, ncols=4, tstep=0):
    """Generator function that scans the output of readlines on the results file \*.LIST
    to return an numpy array dtype float' with the results for the current time step

    Parameters
    ----------
    lst_file : str or list of str
        the results file as read with readlines() or the path to the file
    marker : str
        the first part of the fil,
    tstep : int
        number of first time step --> the identifier of the WELL/DRN file is build from this"""

    if isinstance(lst_file, str):
        with open(lst_file, 'r') as fid:
            list_file = fid.readlines()
    elif isinstance(lst_file, Omo_setup):
        tmp = os.path.join(lst_file.modelfolder, lst_file.modelname + '.list')
        with open(tmp, 'r') as fid:
            list_file = fid.readlines()
    else:
        list_file = lst_file

    filename_well = marker + make_shape_two(tstep, length=4) + '.dat'
    start_at_line = -1
    marker_array = np.zeros((nrows, ncols))
    read = False

    for lnr, line in enumerate(list_file):
        ### start
        if filename_well in line:
            start_at_line = lnr + 4
            #print('startline', start_at_line)
        if lnr == start_at_line:
            read = True
            #print('read true at', lnr)
        if read:
            #print(lnr - start_at_line)
            marker_array[lnr - start_at_line, :] = [float(x) for x in line.split()[1:]]
        if lnr == start_at_line + nrows-1:
            ### flags nrows
            read = False
            tstep += 1
            start_at_line = -1
            filename_well = marker + make_shape_two(tstep, length=4) + '.dat'
            ### get out to return the well array for the current time step
            yield marker_array



def lstfile_iter_balance(setup_mf, period_number=1, style='numpyarray'):
    """Generator function that scans the output of readlines on the results file \*.LIST
    to return an numpy array dtype float' with the results for the current time step

    Parameters
    ----------
    lst_file :  Omo_setup
        the results file as read with readlines() or the path to the file
    tstep : int
        number of first time step --> the identifier of the WELL/DRN file is build from this
    style : str
        numpyarray or dict

    Returns
    -------
    an iterator with returns a numpy array if style is numpyarray or a dictionary if syle is dict"""
    tstep = make_shape_two(1, length=5, filler=' ')
    period = make_shape_two(period_number, length=4, filler=' ')
    marker = 'VOLUMETRIC BUDGET FOR ENTIRE MODEL AT END OF TIME STEP{}, STRESS PERIOD{}'.format(tstep,
                                                                                                period)
    # print('-->', period_number, period, tstep)
    tmp = os.path.join(setup_mf.modelfolder, setup_mf.modelname + '.list')
    with open(tmp, 'r') as fid:
            list_file = fid.readlines()

    start_at_line = -1
    COL = 12
    if style == 'numpyarray':
        marker_array = np.zeros((COL, 2))
    else:
        marker_array = {}
        fields=['STORAGE IN', 'CONSTANT HEAD IN', 'WELLS IN', 'DRAINS IN', 'RECHARGE IN', 'TOTAL IN',
                'STORAGE OUT', 'CONSTANT HEAD OUT', 'WELLS OUT', 'DRAINS OUT', 'RECHARGE OUT', 'TOTAL IN',]
    read = False
    cnt = 0
    for lnr, line in enumerate(list_file):
        #if lnr == 28634:
        #    print('..................')
        ### start
        if marker in line:
            #print('-->', period_number-1, period_number, '...',tstep)
            start_at_line = lnr + 8
            #print('startline', start_at_line)
        if lnr == start_at_line:
            read = True
            #print('read true at', lnr)
        if read:
            #print(lnr - start_at_line)
            tmp = line.split()
            if len(tmp) > 4:
                try:
                    for y in enumerate([2, 5]):
                        if style == 'numpyarray':
                            marker_array[cnt, y[0]] = float(tmp[y[1]])
                        else:
                            marker_array.update({fields[cnt]: float(tmp[y[1]])})
                    cnt += 1
                except:
                    for y in enumerate([3, 7]):
                        if style == 'numpyarray':
                            marker_array[cnt, y[0]] = float(tmp[y[1]])
                        else:
                            marker_array.update({fields[cnt]: float(tmp[y[1]])})
                    cnt += 1

        if lnr == start_at_line + 16 and read:
            ### flags nrows
            read = False
            cnt = 0
            period_number += 1
            period = make_shape_two(period_number, length=4, filler=' ')
            marker = 'VOLUMETRIC BUDGET FOR ENTIRE MODEL AT END OF TIME STEP{}, STRESS PERIOD{}'.format(tstep,
                                                                                                        period)
            ### get out to return the well array for the current time step
            if marker_array:
                yield marker_array


if __name__ == "__main__":
    pass
