#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Functions for well and well group mananagement.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import os
import numpy as np
import pandas as pd
from mooflow.classes import (Register, Well, Wellgroup)
from mooflow.utils import (make_shape_two, load_obj)

def read_well_file(registry, timestep, ts_format=4):
    filename_well = 'WEL_' + make_shape_two(timestep, length=ts_format) +'.dat'   
    file_str_well = os.path.join(registry.inputfolder, filename_well)
    well_array = np.loadtxt(file_str_well)
    return well_array


def wells_and_wellgroups(setup_mf, wellgroupfile, patternfile, 
delimiter_wg=';', delimiter_pf=';',factor=1, timesteps=None):
    '''creates registries with the wells and well_goups.
    
    Paramters
    ---------
    wellgroupfile : str
        the csv file with the settings for 
        Index - the well ids; 
        x_pal - geographic location x (optional); 
        y_pal - geographic location y (optional);
        layer - cell location z; 
        row - cell location x
        column- cell location y; 
        p_class - classifier --> which pattern to assign from the patternfile
        min - the minimum for rescaling of the pattern (optional);
        max - the maximum for rescaling of the pattern (optional);
        well_group - well group; 
        active_node - assign to this active node in the pywr model
    patternfile : str
        a pickled pandas DataFrame with columns 1:12 for the monthly pattern
        and rows p_class_0 to p_class_n fot the n different pattern styles
    delimiter_wg : str
        the delimiter used int the wellgroupfile
    delimiter_pf : str
        the delimiter used int the patternfile (if not pkl file)
    factor : float
        scale the extraction/injection rates
    timesteps : list with the same length of saisons in the patterns.
        values are last day of each season.
        default is [ 31,  59,  90, 120, 151, 181, 212, 243, 273, 304, 334, 365]
        for 12 saison. 
        
    Returns
    -------
    list with the register for wellgroup and wells'''
    if patternfile[-3:] == 'pkl':
        #raise NotImplementedError('Only pkl files are accepted. 1:12, p_class_0, ..., p_class_n')
        p_classes = load_obj(patternfile)
    else:
        p_classes = pd.read_csv(patternfile, 
                         header=[0],
                         delimiter=delimiter_pf,
                         index_col=0)
    coords = pd.read_csv(wellgroupfile, 
                         header=[0],
                         delimiter=delimiter_wg,
                         index_col=0)
    
    reg_well = Register('Well')
    reg_wellgroup = Register('Wellgroup')
    
    ### well groups listed in file
    unique_wc = np.unique(coords.loc[:, 'well_group'])

    for uwc in unique_wc:
        wc = Wellgroup('wc' + str(uwc))
        reg_wellgroup.add(wc)
        
    if timesteps is None:
        if "days" in p_classes:
            tsl = list(p_classes.loc[:, 'days'].values)
        else:
            tsl = [ 31,  59,  90, 120, 151, 181, 212, 243, 273, 304, 334, 365]
    
    ### wells listed in file
    for well in coords.index:
        pc = 'p_class_' + str(coords.loc[well, 'p_class'])
        if 'max' in list(coords):
            pattern = p_classes.loc[:, pc].values * factor\
                * (coords.loc[well, 'max'] - coords.loc[well, 'min']) \
                + coords.loc[well, 'min']
        else:
            pattern = list(p_classes.loc[:, pc])
        w = Well(well, 
                 tsl, 
                 pattern)
        w.location = {'x': coords.loc[well, 'row'], 
                      'y': coords.loc[well, 'column'], 
                      'z': coords.loc[well, 'layer']}
        if setup_mf.usePywr:
            w.pywr_node = coords.loc[well, 'active_node']
        reg_well += (w)
        uwc = coords.loc[well, 'well_group']
        wc = reg_wellgroup['wc' + str(uwc)]
        wc.add_wells(w)
    return reg_wellgroup, reg_well


def boundaries(boundfile, reg_wellgroup, delimiter=';', factor=1):
    '''read the lower and upper boundaries from a csv file
    
    Parameters
    ----------
    boundfile : str
        the csv file with the boundary information
    reg_wellgroup : mooflow.classes.Registry object
        the registry for the wellgroups
    delimiter : str, optional
        the delimiter used in the boundfile'''
    bound = pd.read_csv(boundfile, 
                        header=[0],
                        delimiter=delimiter,
                        index_col=[0,1])

    for idx in bound.index:
        bvecl = bound.loc[(idx, 'lower'),:].values * factor
        bvecu = bound.loc[(idx, 'upper'),:].values * factor
        non_nan = np.shape(bvecu)[1] - np.sum(np.isnan(bvecu))
        idxs = 'wc' + str(idx[0])
        if non_nan > 1:
            bvecl = bvecl.tolist()
            bvecu = bvecu.tolist()
        else:
            bvecl = bvecl[0]
            bvecu = bvecu[0]
        reg_wellgroup[idxs].set_l_u_boundaries(bvecl[0], bvecu[0])

        if non_nan == 1:
            reg_wellgroup[idxs].num_par = -1
        else:
            reg_wellgroup[idxs].num_par = len(bvecu) - np.sum(~(bvecu == -999))
    return reg_wellgroup
