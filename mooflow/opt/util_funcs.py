# -*- coding: utf-8 -*-
import numpy

def distance(feasible_ind, original_ind):
    """A distance function to the feasibility region."""
    return sum((f - o)**2 for f, o in zip(feasible_ind, original_ind))


def closest_feasible(individual):
    """A function returning a valid individual from an invalid one."""
    feasible_ind = numpy.array(individual)
    feasible_ind = numpy.maximum(0, feasible_ind)
    feasible_ind = numpy.minimum(1, feasible_ind)
    return feasible_ind


def valid(individual):
    """Determines if the individual is valid or not."""
    if any(individual < 0) or any(individual > 1):
        return False
    return True

