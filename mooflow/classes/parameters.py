# -*- coding: utf-8 -*-
"""
The optimization parameters.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""
import collections
import numpy as np
from mooflow.classes import Wellgroup


class Parameters:
    def __init__(self, parametervector):
        """Manage the optimization parameters."""
        if not isinstance(parametervector, collections.Iterable):
            raise TypeError('parametervector has to an iterable.')
        self.parvec = parametervector
        self.__counter = 0


    def rescale_group(self, group):
        '''Set the multiplier for all wells in the group.

        Parameters
        ----------
        group : Wellgroup object
            the ID or the Well object to search for'''
        if isinstance(group, Wellgroup):
            self.rescale_for_wellgroup(group)


    def rescale_for_wellgroup(self, group):
        '''Rescale the multiplier for all wells in a wellgroup, using the
        lower and upper boundary levels in the well group. if a upper
        boundary is -999, no parameter is read and the multiplier is 1.0.

        Parameters
        ----------
        group : Wellgroup object
            recale the multiplier for this well group'''
        if not isinstance(group, Wellgroup):
            raise TypeError('Wrong type for group. Wellgroup expected, got {}'.format(
                    type(group)))
        #print(group.id)
        l_bound = group.get_settings('lbound')
        u_bound = group.get_settings('ubound')
        #if group.id == "wcInf_211IS":
        #    print(2)

        if l_bound is None or u_bound is None:
            raise ValueError('No lower and upper boundaries given for {}.'.format(
                    group.id))

        if not len(l_bound) or not len(u_bound):
            raise ValueError('Please set the lower and upper boundaries for {}.'.format(
                    group.id))

        len_b = len(l_bound)
        scaled_par = np.ones(len_b)
        for i, val in enumerate(u_bound):
            if not val == -999:
                scaled_par[i] = self.parvec[self.__counter] * (val - l_bound[i]) + l_bound[i]
                ### advance the parameter by one for each bound
                if group.num_par > 0:
                    self.__counter += 1
        ### in case of an constant boundary, advance the parameter only once
        if group.num_par == -1:
            self.__counter += 1

        for well in group.get_settings('wells'):
            well.multiplier = scaled_par


    def __getitem__(self, index):
        if isinstance(index, slice):
            return self.parvec[index]
        elif isinstance(index, int):
            return self.parvec[index]

    def __len__(self):
        return len(self.parvec)


if __name__ == "__main__":
    pass
